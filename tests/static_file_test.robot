*** Settings ***
Documentation     Test that static files are served correctly.
Library           SeleniumLibrary

Suite Setup       Open Browser To Homepage
Suite Teardown    Close Browser
Test Setup        Go To Homepage
Test Teardown     Close Browser

*** Variables ***
${HOME_PAGE}      http://localhost:8000/  # Adjust this to your Django app's URL
${STATIC_FILE}    http://localhost:8000/static/css/style.css  # Adjust the path to your static file

*** Keywords ***
Open Browser To Homepage
    Open Browser    ${HOME_PAGE}    browser=Chrome  # Or your browser of choice

Go To Homepage
    Go To    ${HOME_PAGE}

Check Static File
    [Arguments]    ${file_url}
    Go To    ${file_url}
    ${status_code}=    Get Location
    Should Not Contain    ${status_code}    Not Found
    Should Not Contain    ${status_code}    Error

*** Test Cases ***
Static File Should Be Accessible
    [Documentation]    Test if a specific static file is served correctly.
    Check Static File    ${STATIC_FILE}

